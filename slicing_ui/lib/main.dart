import 'package:flutter/material.dart';
import 'package:slicing_ui/slicing_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Slicing Ui',
      home: Slicing(),
      debugShowCheckedModeBanner: false,
    );
  }
}
