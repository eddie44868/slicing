import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:rating_bar/rating_bar.dart';

class Slicing extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.grey[200],
        body: Stack(
          children: [
            Padding(
                  padding: const EdgeInsets.fromLTRB(8, 30, 8, 30),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      ElevatedButton(
                        onPressed: () {},
                        child: Icon(
                          Icons.arrow_back_ios_new_rounded,
                          color: Colors.black,
                        ),
                        style: ButtonStyle(
                          shape: MaterialStateProperty.all(CircleBorder()),
                          padding: MaterialStateProperty.all(EdgeInsets.all(10)),
                          backgroundColor:
                              MaterialStateProperty.all(Colors.white),
                        ),
                      ),
                      ElevatedButton(
                        onPressed: () {},
                        child: Icon(
                          Icons.favorite,
                          color: Colors.pink,
                        ),
                        style: ButtonStyle(
                          shape: MaterialStateProperty.all(CircleBorder()),
                          padding: MaterialStateProperty.all(EdgeInsets.all(10)),
                          backgroundColor:
                              MaterialStateProperty.all(Colors.white),
                        ),
                      )
                    ],
                  ),
                ),
            SingleChildScrollView(
              child: Column(children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(8, 80, 8, 40),
                  child: Center(
                    child: Image(
                      image: AssetImage('assets/pic.png'),
                      height: 260,
                      width: 260,
                    ),
                  ),
                ),
                Material(
                  elevation: 4,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(35),
                      topRight: Radius.circular(35)),
                  child: Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        SizedBox(
                          height: 10,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            ClipRRect(
                              borderRadius: BorderRadius.circular(30.0),
                              child: Container(
                                alignment: Alignment.center,
                                color: Colors.red[100],
                                width: 60,
                                height: 20,
                                child: Text(
                                  'Comic',
                                  style: TextStyle(
                                      fontSize: 12, color: Colors.red),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            ClipRRect(
                              borderRadius: BorderRadius.circular(30.0),
                              child: Container(
                                alignment: Alignment.center,
                                color: Colors.red[100],
                                width: 60,
                                height: 20,
                                child: Text(
                                  'Fantasy',
                                  style: TextStyle(
                                      fontSize: 12, color: Colors.red),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(8, 15, 8, 15),
                          child: Text(
                            'Pulang Pergi',
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.bold),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(8, 5, 8, 20),
                          child: Text(
                            'By Tere Liye',
                            style: TextStyle(
                                fontSize: 17, fontWeight: FontWeight.w300),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SizedBox(
                                child: RatingBar.readOnly(
                              initialRating: 4,
                              filledIcon: Icons.star,
                              emptyIcon: Icons.star_border,
                              halfFilledIcon: Icons.star_half,
                              isHalfAllowed: true,
                              filledColor: Colors.amber,
                              emptyColor: Colors.amberAccent,
                              halfFilledColor: Colors.amberAccent,
                              size: 25,
                            )),
                            SizedBox(
                              width: 10,
                            ),
                            RichText(
                              text: TextSpan(
                                text: '4.8',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16,
                                    color: HexColor("#00C1D4")),
                                children: const <TextSpan>[
                                  TextSpan(
                                      text: '/5.0 (150 Review)',
                                      style: TextStyle(
                                          fontWeight: FontWeight.w200,
                                          fontSize: 16,
                                          color: Colors.black)),
                                ],
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(8, 15, 240, 10),
                          child: Text(
                            'Sinopsis',
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.bold),
                            textAlign: TextAlign.start,
                          ),
                        ),
                        Container(
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(30, 5, 30, 15),
                            child: Text(
                              '"Aku tahu sekarang, lebih banyak luka di hati bapakku dibanding di tubuhnya. Juga mamakku, lebih banyak tangis di hati Mamak dibanding di matanya."Sebuah kisah tentang perjalanan pulang, melalui pertarungan demi pertarungan, untuk memeluk erat semua kebencian dan rasa sakit."',
                              style: TextStyle(
                                  fontSize: 16, fontWeight: FontWeight.w200),
                              textAlign: TextAlign.justify,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                )
              ]),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 15),
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    ElevatedButton(
                      onPressed: () {},
                      child: Container(
                        alignment: Alignment.center,
                        width: 150,
                        height: 50,
                          child: Text(
                        'Read Now',
                        style: TextStyle(
                            fontSize: 25,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                      )),
                      style: ButtonStyle(
                          shape: MaterialStateProperty.all(RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(50.0))),
                          padding: MaterialStateProperty.all(EdgeInsets.all(10)),
                          backgroundColor:
                              MaterialStateProperty.all(HexColor("#00C1D4"))),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ));
  }
}
